#!/bin/sh

MYDIR=$(dirname "$(readlink -f "$0")")

# Start Wifi AP Toggle
killall wpa_supplicant
/jci/scripts/jci-fw.sh stop
/jci/scripts/jci-wifiap.sh start
/jci/scripts/jci-wifiap.sh status > ${MYDIR}/wifi.log
